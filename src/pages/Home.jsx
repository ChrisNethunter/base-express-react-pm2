
import React, { Component } from 'react';
import Auth from '../controllers/Auth';
import '../styles/App.css';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {}
    }

    componentDidMount(){
        Auth.login( { test : 1 },( response ) => {
            console.log({response})
        });
    }

	render() {
		return (
            <div className="App">
                <header className="App-header">
                    <img src="/logo.svg" className="App-logo" alt="logo" />
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                </header>
            </div>
		);
	}
}

export default Home;
