import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

export default class MainLayout extends React.Component {
    constructor(props) {
		super(props);
		this.state = {};
    }
    render() {
        return (
            <div className="content-layout">
                <React.Fragment>    
                   {/*  <NavbarNode/> */}
                    {this.props.children}
                    {/*    <h1> Footer </h1> */}
                </React.Fragment>
            </div>            
        )
    }
}
